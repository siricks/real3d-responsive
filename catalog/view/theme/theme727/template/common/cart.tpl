<div class="box-cart">
	<div id="cart" class="cart toggle-wrap">

		<button type="button" class="hidden-xs" style="color: white; font-size: 2.5rem; padding-top: 2px; font-family: 'Droid Sans', sans-serif, Verdana">
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEqSURBVEhL7ZUxagJhFIS3ScA2oEUQAraayiPY2CuSI+QUgkWuYGstIVZWWqRIF7ARIQdICCRVClvXfI+dXdzEGNx92/nBsDrv/TMo8huccCEMwx6aofmO7H1XKz4Q+Ljdg/la8YHAJ2WnMF8rPhz4RO5FD8pOUUTRnbJT4E+14gOBbWUn4K1QRSs+kHtG6GdUUVBJDMEDlWx41GT7Q8EF+lDZUHYxUNBTkdGWXQwUTKwMvnh9Ldsfwu0rXFoTz1d0pZE/hFetRGXvPJoaJeBX0ATlu4g50EBvKlujG41sVkYrm/0E//iLmEOX6FkZFnKP6mhvicEs27XFwRIaKedfMhfFENBCL8r7k9xFBjnnBN2iRRT7G2a+f5ZkNgntozHa/dV1tHIiD0HwDS03SbV5wfQoAAAAAElFTkSuQmCC">
			8 812 384 69 16
		</button>

		<ul class="pull-right toggle_cont">
			<li>
				<h4><?php echo $text_shopping_cart; ?></h4>
			</li>
			<?php if ($products || $vouchers) { ?>
			<li>
				<table class="table">
					<?php foreach ($products as $product) { ?>
					<tr>
						<td class="text-center"><?php if ($product['thumb']) { ?>
							<div class="image">
								<a href="<?php echo $product['href']; ?>">
									<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
								</a>
							</div>
							<?php } ?>
						</td>
						<td class="text-left">
							<div class="name">
								<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
							</div>
						</td>
						<td class="text-right"><div><span class="price-cart"><i> x </i> <?php echo $product['quantity']; ?> <b><?php echo $product['total']; ?></b></span></div></td>
						<td class="text-center">
							<button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn-danger btn-xs">
								<i class="fa fa-times"></i>
							</button>
						</td>
					</tr>
					<?php } ?>
					<?php foreach ($vouchers as $voucher) { ?>
					<tr>
						<td class="text-center"></td>
						<td class="text-left"><?php echo $voucher['description']; ?></td>
						<td class="text-right">x&nbsp;1</td>
						<td class="text-right"><?php echo $voucher['amount']; ?></td>
						<td class="text-center text-danger">
							<button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs">
								<i class="fa fa-times"></i>
							</button>
						</td>
					</tr>
					<?php } ?>
				</table>
			</li>
			<li>
				<table class="table total">
					<?php foreach ($totals as $total) { ?>
					<tr>
						<td class="text-left">
							<?php echo $total['title'] .' : ' ; ?> <strong><?php echo $total['text']; ?></strong>
						</td>
					</tr>
					<?php } ?>
				</table>
				<div class="text-left">
					<a class="btn" href="<?php echo $cart; ?>"><span> <?php echo $text_cart; ?></span></a>
					<a class="btn-primary" href="<?php echo $checkout; ?>"><span ><?php echo $text_checkout; ?></span></a>
				</div>
			</li>
			<?php } else { ?>
			<li>
				<p class="text-left"><?php echo $text_empty; ?></p>
			</li>
			<?php } ?>
		</ul>
	</div>
</div>