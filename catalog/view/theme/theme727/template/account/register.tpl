<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger">
        <i class="fa fa-exclamation-circle"></i>
        <?php echo $error_warning; ?>
        <button type="button" class="close material-design-close47"></button>
    </div>
    <?php } ?>
    <div class="row"><?php
    if (isset($column_left)){
    echo $column_left;
    }

     ?>
        <?php if (isset($column_left) && isset($column_right)) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif (isset($column_left) || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php }

            if( empty($column_left) && empty($column_right)) {
                $class = 'col-sm-12';
            }

        ?>
        <div id="content" class="col-sm-offset-2 col-sm-8"><?php echo $content_top; ?>
            <h1><?php echo $heading_title; ?></h1>
            <div class="row">
                <div class="col-xs-12 text-xs-center no-gutter-xs"><p><?php echo $text_account_already; ?></p></div>
                <p class="text-xs-center">Выберите тип регистрации:</p>
                <style>
                    .vids li a {
                        padding-right: 5px;
                        font-size: 13px;
                    }
                </style>

                <!-- Виды регистрации -->
                <ul class="nav nav-tabs vids" role="tablist">
                    <li role="presentation"><a id="close_ur" style="padding-left: 0">Регистрация физического лица</a></li>
                    <li role="presentation"><a  style="    padding-left: 20px;" aria-controls="address" role="tab" data-toggle="collapse"
                                               aria-expanded="false" id="open_ur">Регистрация юридического лица</a></li>
                    <li role="presentation"><a href="#like_ip" style="padding-right: 0;" aria-controls="like_ip" role="tab" data-toggle="collapse"
                                               aria-expanded="false" id="open_ip">Регистрация индивидуального предпринимателя</a></li>
                </ul>
            </div>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                  class="register-form row form-horizontal tab-content">


                <div class="col-sm-12">
                    <!--*******Основные данные********-->
                    <!-- +++ Имя--><!--Фамилия--><!--Отчество-->
                    <!--  +++ E-mail:--><!--Пароль--><!--Подтверждение пароля-->
                    <!-- ++ Телефон--><!--Предполагаемый оборот-->
                    <fieldset class="row" id="main-info">
                        <div class="col-sm-4 mt-10">
                            <legend>Основные данные</legend>
                        </div>
                        <!-- +++ Имя--><!--Фамилия--><!--Отчество-->
                        <div class="col-sm-12">
                            <div class="row">
<style>


</style>
                                <!--Имя-->
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row">
                                        <label class="col-sm-12 label-required" for="input-firstname"> Имя </label>
                                        <input class="col-sm-12" type="text" name="firstname"
                                               value="<?php echo $firstname; ?>" id="input-firstname" required>
                                        <?php if ($error_firstname) { ?>
                                        <div class="text-danger"><?php echo $error_firstname; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <!--Фамилия-->
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row row-with-select">
                                        <label class="col-sm-12 label-required" for="input-lastname">Фамилия</label>
                                        <input class="col-sm-12" type="text" name="lastname"
                                               value="<?php echo $lastname; ?>" id="input-lastname" required>
                                        <?php if ($error_lastname) { ?>
                                        <div class="text-danger"><?php echo $error_lastname; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <!--Отчество-->
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row">
                                        <label class="col-sm-12 label-required" for="input-secondname">Отчество</label>
                                        <input class="col-sm-12" type="text" name="secondname"
                                               value="<?php echo $secondname; ?>" id="input-secondname" required>
                                        <?php if ($error_secondname) { ?>
                                        <div class="text-danger"><?php echo $error_secondname; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--  +++ E-mail:--><!--Пароль--><!--Подтверждение пароля-->
                        <div class="col-sm-12">
                            <div class="row">
                                <!--E-mail:-->
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row">
                                        <label class="col-sm-12 label-required" for="input-email">E-mail:</label>
                                        <input required class="col-sm-12" type="email" name="email" value="<?php echo $email; ?>"
                                               id="input-email">
                                        <?php if ($error_email) { ?>
                                        <div class="text-danger"><?php echo $error_email; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!--Пароль-->
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row">
                                        <label class="col-12 label-required" for="input-password">Пароль</label>
                                        <input class="col-12" type="password" name="password"
                                               value="<?php echo $password; ?>" id="input-password" required>
                                        <?php if ($error_password) { ?>
                                        <div class="text-danger"><?php echo $error_password; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!--Подтверждение пароля-->
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row">
                                        <label class="col-sm-12 label-required" for="input-confirm">Подтверждение пароля</label>
                                        <input class="col-sm-12" type="password" name="confirm"
                                               value="<?php echo $confirm; ?>" id="input-confirm" required>
                                        <?php if ($error_confirm) { ?>
                                        <div class="text-danger"><?php echo $error_confirm; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ++ Телефон--><!--Предполагаемый оборот-->
                        <div class="col-sm-12">
                            <div class="row">
                                <!-- Телефон -->
                                <div class="col-sm-6 px-30 my-10">
                                    <div class="row">
                                        <label class="col-sm-12 label-required" for="input-telephone">Телефон</label>
                                        <input required class="col-sm-12" type="tel" id="input-telephone"
                                               value="<?php echo $telephone; ?>" placeholder="+7 (987) 123-45-67"
                                               name="telephone">
                                        <?php if ($error_telephone) { ?>
                                        <div class="text-danger"><?php echo $error_telephone; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <!--Предполагаемый оборот-->
                                <div class="col-sm-6 px-30 my-10">
                                    <div class="row">
                                        <label class="col-sm-12" for="estimated_turnover">Предполагаемый оборот</label>
                                        <input class="col-sm-12" type="text" value="<?php echo $estimated_turnover; ?>"
                                               name="estimated_turnover" id="estimated_turnover">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                    <div class="row collapse" id="address">
                        <div class="col-sm-12">

                            <!--*******Реквизиты организации********-->
                            <!--+++Наименование--><!--Форма организации--><!--Вид деятельности-->
                            <!--+++ ИНН --><!-- КПП --><!-- ОГРН -->
                            <fieldset class="row" id="organization-requisites">
                                <div class="col-sm-4 mt-10">
                                    <legend>Реквизиты организации</legend>
                                </div>

                                <!--+++Наименование--><!--+++Форма организации--><!--+++Окпо-->
                                <div class="col-sm-12" id="naimenovine_inputs">
                                    <div class="row">

                                        <!--Наименование-->
                                        <div class="col-sm-12 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-denomination">Наименование</label>
                                                <input class="col-sm-12" type="text"
                                                       value="<?php echo $denomination; ?>" name="denomination"
                                                       id="input-denomination">
                                            </div>
                                        </div>

                                        <!--Форма организации+-->
                                        <div class="col-sm-12 col-md-4 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-company-type1">Форма
                                                    организации</label>
                                                <select class="col-sm-12" name="company_type" id="input-company-type">
                                                    <option value="1">Выберите форму организации</option>
                                                    <option value="ЗАО">ЗАО</option>
                                                    <option value="ООО">ООО</option>
                                                    <option value="ТОО">ТОО</option>
                                                    <option value="АО">ОАО</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!--ОКПО-->
                                        <div class="col-sm-12 col-md-4 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12" for="input-okpo">ОКПО</label>
                                                <input class="col-sm-12" type="text" name="okpo" value="<?php echo $okpo; ?>"
                                                       id="input-okpo">
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <!--+++ ИНН --><!-- КПП --><!-- ОГРН -->
                                <div class="col-sm-12">
                                    <div class="row">

                                        <!-- ИНН -->
                                        <div class="col-sm-6 col-md-4 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-inn">ИНН</label>
                                                <input class="col-sm-12" type="text" name="inn"
                                                       value="<?php echo $inn; ?>" id="input-inn">
                                            </div>
                                        </div>
                                        <!-- КПП -->
                                        <div class="col-sm-6 col-md-4 px-30 my-10"  id="kpp_inputs">
                                            <div class="row">
                                                <label class="col-sm-12" for="input-kpp">КПП</label>
                                                <input class="col-sm-12" value="<?php echo $kpp; ?>" type="text"
                                                       name="kpp" id="input-kpp">
                                            </div>
                                        </div>
                                        <!-- ОГРН -->
                                        <div class="col-sm-6 col-md-4 px-30 my-10" id="ogrn_inputs">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-ogrn">ОГРН</label>
                                                <input class="col-sm-12" type="text" name="ogrn"
                                                       value="<?php echo $ogrn; ?>" id="input-ogrn">
                                            </div>
                                        </div>

                                        <!-- ОГРНИП -->
                                        <div class="col-sm-6 col-md-4 px-30 my-10" id="ogrnip_inputs">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-ogrnip">ОГРНИП</label>
                                                <input class="col-sm-12" type="text" name="ogrnip"
                                                       value="<?php echo $ogrnip; ?>" id="input-ogrnip">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </fieldset>

                            <!--*******Юридический адрес********-->
                            <!--++Страна--><!--Город-->
                            <!--++Юридический Адрес--> <!--Индекс-->
                            <fieldset class="row" id="urid_address">
                                <div class="col-sm-4 mt-10">
                                    <legend>Юридический адрес</legend>
                                </div>

                                <div class="col-sm-12">
                                    <div class="row">

                                        <!--Страна-->
                                        <div class="col-sm-6 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-country">Страна</label>
                                                <select name="country_id" id="input-country" class="col-sm-12">
                                                    <option value=""><?php echo $text_select; ?></option>
                                                    <?php foreach ($countries as $country) { ?>
                                                    <?php if ($country['country_id'] == $country_id) { ?>
                                                    <option value="<?php echo $country['country_id']; ?>"
                                                            selected="selected"><?php echo $country['name']; ?></option>
                                                    <?php } else { ?>
                                                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                                    <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <?php if ($error_country) { ?>
                                                <div class="text-danger"><?php echo $error_country; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <!--Город-->
                                        <div class="col-sm-6 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-city">Город</label>
                                                <input class="col-sm-12" type="text" name="city"
                                                       value="<?php echo $city; ?>" id="input-city">
                                                <?php if ($error_city) { ?>
                                                <div class="text-danger"><?php echo $error_city; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="row">

                                        <!--Юридический Адрес-->
                                        <div class="col-sm-6 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-address-1">Юридический Адрес</label>
                                                <input class="col-sm-12" type="text" name="address_1"
                                                       value="<?php echo $address_1; ?>" id="input-address-1">
                                                <?php if ($error_address_1) { ?>
                                                <div class="text-danger"><?php echo $error_address_1; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <!--Индекс-->
                                        <div class="col-sm-6 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-postcode">Индекс:</label>
                                                <input class="col-sm-12" type="text" name="postcode"
                                                       value="<?php echo $postcode; ?>" id="input-postcode">
                                                <?php if ($error_postcode) { ?>
                                                <div class="text-danger"><?php echo $error_postcode; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </fieldset>

                            <!--*******Фактический адрес********-->
                            <!--Совпадает с юридическим адресом-->
                            <!--++Страна--><!--Город-->
                            <!--++Фактический адрес--> <!--Индекс-->
                            <fieldset class="row" id="factical_address">
                                <div class="col-sm-4 mt-10">
                                    <legend>Фактический адрес</legend>
                                </div>

                                <div class="col-sm-6" style="margin-top: 50px">
                                    <div class="row">
                                        <input class="col-sm-3" name="same_with_ur" id="same-with-ur"
                                                            type="checkbox">
                                        <label class="col-sm-9" for="same-with-ur">Совпадает
                                            с юридическим адресом</label>
                                    </div>
                                </div>

                                <div class="col-sm-12 collapse in" id="fact_addr">
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <div class="row">
                                                <!--Страна-->
                                                <div class="col-sm-6 px-30 my-10">
                                                    <div class="row">
                                                        <label class="col-sm-12 label-required" for="input-country-fact">Страна</label>
                                                        <select name="country_id-fact" id="input-country-fact"
                                                                class="col-sm-12">
                                                            <option value=""><?php echo $text_select; ?></option>
                                                            <?php foreach ($countries as $country) { ?>
                                                            <?php if ($country['country_id'] == $country_id-fact) { ?>
                                                            <option value="<?php echo $country['country_id']; ?>"
                                                                    selected="selected"> <?php  echo $country['name']; ?></option>
                                                            <?php } else { ?>
                                                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                                            <?php } ?>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <!--Город-->
                                                <div class="col-sm-6 px-30 my-10">
                                                    <div class="row">
                                                        <label class="col-sm-12 label-required" for="input-city_fact">Город</label>
                                                        <input class="col-sm-12" type="text" name="city_fact"
                                                               value="<?php echo $city_fact; ?>" id="input-city_fact">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="row">

                                                <!--Фактический адрес-->
                                                <div class="col-sm-6 px-30 my-10">
                                                    <div class="row">
                                                        <label class="col-sm-12 label-required" for="input-address-fact">Фактический
                                                            адрес</label>
                                                        <input class="col-sm-12" type="text" name="address_fact"
                                                               value="<?php echo $address_fact; ?>"
                                                               id="input-address-fact">
                                                    </div>
                                                </div>

                                                <!--Индекс-->
                                                <div class="col-sm-6 px-30 my-10">
                                                    <div class="row">
                                                        <label class="col-sm-12 label-required"
                                                               for="input-postcode_fact">Индекс:</label>
                                                        <input class="col-sm-12" type="text" name="postcode_fact"
                                                               value="<?php echo $postcode_fact; ?>"
                                                               id="input-postcode_fact">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </fieldset>

                            <!--*******Платежные реквизиты*******-->
                            <!--++БИК--><!--+++Наименование банка:-->
                            <!--+++Город банка:--><!--++Корреспондентский счет:--><!--+++Расчетный счет:-->
                            <fieldset class="row" id="requizitus">
                                <div class="col-sm-4 mt-10">
                                    <legend>Платежные реквизиты</legend>
                                </div>

                                <div class="col-sm-12">
                                    <div class="row">

                                        <!--БИК-->
                                        <div class="col-sm-6 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-bic">БИК</label>
                                                <input class="col-sm-12" type="text" name="bic"
                                                       value="<?php echo $bic; ?>" id="input-bic">
                                            </div>
                                        </div>

                                        <!--Наименование банка:-->
                                        <div class="col-sm-6 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-bank">Наименование банка:</label>
                                                <input class="col-sm-12" type="text" name="bank"
                                                       value="<?php echo $bank; ?>" id="input-bank">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="row">
                                        <!--Город банка:-->
                                        <div class="col-sm-6 col-md-4 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-bank_city">Город банка</label>
                                                <input class="col-sm-12" type="text" name="bank_city"
                                                       value="<?php echo $bank_city; ?>" id="input-bank_city">
                                            </div>
                                        </div>


                                        <!--Корреспондентский счет:-->
                                        <div class="col-sm-6  col-md-4 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-ks">Корреспондентский счет</label>
                                                <input class="col-sm-12" type="text" name="ks"
                                                       value="<?php echo $ks; ?>" id="input-ks">
                                            </div>
                                        </div>

                                        <!--Расчетный счет:-->
                                        <div class="col-sm-6 col-md-4 px-30 my-10">
                                            <div class="row">
                                                <label class="col-sm-12 label-required" for="input-rs">Расчетный счет</label>
                                                <input class="col-sm-12" type="text" name="rs"
                                                       value="<?php echo $rs; ?>" id="input-rs">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>
                    <!--******Дополнительная информация*******-->
                    <fieldset class="row">
                        <div class="col-sm-5 mt-10">
                            <legend>Дополнительная информация</legend>
                        </div>

                        <div class="col-sm-12"><div class="row">

                                <!--Пол-->
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row">
                                        <label class="col-sm-12">Пол</label>
                                        <select name="sex" id="input-sex">
                                            <option selected disabled value="">Пол</option>
                                            <option value="Мужской">Мужской</option>
                                            <option value="Женский">Женский</option>
                                        </select>
                                    </div>
                                </div>

                                <!--Дата рождения-->
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row">
                                        <label class="col-sm-12" for="input-birthday">Дата рождения</label>
                                        <input class="col-sm-12" type="date" value="<?php echo $birthday; ?>"
                                               name="birthday" id="input-birthday">
                                    </div>
                                </div>

                                <!--Вид деятельности+-->
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row">

                                        <label class="col-sm-12" for="input-activity-type">Вид
                                            деятельности</label>
                                        <select name="activity_type" id="input-activity-type">
                                            <option selected disabled value="">Выберите вид деятельности
                                            </option>
                                            <option value="СТО">СТО</option>
                                            <option value="АПТ">АПТ</option>
                                            <option value="Магазин">Магазин</option>
                                            <option value="Сеть магазинов">Сеть магазинов</option>
                                            <option value="Производитель техники">Производитель техники</option>
                                        </select>
                                    </div>
                                </div>

                         </div></div>

                        <div class="col-sm-12">
                            <div class="row">
                                <!--Skype-->
                                <div class="col-sm-4 px-30 my-10" id="skype-inputs">
                                    <div class="row">
                                        <label class="col-sm-12">Skype</label>
                                        <input class="col-sm-12" type="text" value="<?php echo $skype; ?>"
                                               id="input-skype" name="skype">
                                    </div>
                                </div>

                                <!--ICQ-->
                                <div class="col-sm-4 px-30 my-10" id="icq-inputs">
                                    <div class="row">
                                        <label class="col-sm-12" for="input-icq">ICQ</label>
                                        <input class="col-sm-12" value="<?php echo $icq; ?>" type="text" name="icq"
                                               id="input-icq">
                                    </div>
                                </div>



                                <!--Документы-->
                                <div class="col-sm-8 px-30 my-10" id="doc_request_ur" style="display: none">
                                    <p><strong>Необходимо прикрепить документы:</strong></p>
                                    <p>1) Скан паспорта; 2) Свидетельство ОГРН; 3) Свидетельство ИНН/КПП;</p>
                                </div>
                                <div class="col-sm-8 px-30 my-10" id="doc_request_ip" style="display: none">
                                    <p><strong>Необходимо прикрепить документы:</strong></p>
                                    <p> 1) Скан паспорта; 2) Свидетельство ОГРНИП; 3) Свидетельство ИНН/КПП;</p>
                                </div>
                                <div class="col-sm-4 px-30 my-10">
                                    <div class="row">
                                        <label class="col-sm-12">Добавить документы</label>
                                        <input class="col-sm-12" id="download-documents-inputs" type="file" name="documets[]" multiple>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-12 px-30">
                            <div class="row"><label class="col-12  my-10" for="sendtext">Отправить
                                    сообщение</label><textarea id="sendtext" name="sendtext" class="col-12"
                                                               rows="4"></textarea></div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-sm-12">
                    <fieldset>


                        <div class="form-group "
                             style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
                            <label class="col-sm-2 control-label"><?php echo $entry_customer_group; ?></label>
                            <div class="col-sm-10">
                                <?php foreach ($customer_groups as $customer_group) { ?>
                                <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="customer_group_id"
                                               value="<?php echo $customer_group['customer_group_id']; ?>"
                                               checked="checked"/>
                                        <?php echo $customer_group['name']; ?></label>
                                </div>
                                <?php } else { ?>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="customer_group_id"
                                               value="<?php echo $customer_group['customer_group_id']; ?>"/>
                                        <?php echo $customer_group['name']; ?></label>
                                </div>
                                <?php } ?>
                                <?php } ?>
                            </div>
                        </div>


                        <?php foreach ($custom_fields as $custom_field) { ?>
                        <?php //if ($custom_field['location'] == 'account') { ?>
                        <?php if ($custom_field['type'] == 'select') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>"
                             class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <select name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]"
                                        id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                        class="form-control">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                    <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
                                    <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                            selected="selected"><?php echo $custom_field_value['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'radio') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>"
                             class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div>
                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                    <div class="radio">
                                        <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
                                        <label>
                                            <input type="radio"
                                                   name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]"
                                                   value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                   checked="checked"/>
                                            <?php echo $custom_field_value['name']; ?></label>
                                        <?php } else { ?>
                                        <label>
                                            <input type="radio"
                                                   name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]"
                                                   value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                            <?php echo $custom_field_value['name']; ?></label>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'checkbox') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>"
                             class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div>
                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                    <div class="checkbox">
                                        <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $register_custom_field[$custom_field['custom_field_id']])) { ?>
                                        <label>
                                            <input type="checkbox"
                                                   name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]"
                                                   value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                   checked="checked"/>
                                            <?php echo $custom_field_value['name']; ?></label>
                                        <?php } else { ?>
                                        <label>
                                            <input type="checkbox"
                                                   name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]"
                                                   value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                            <?php echo $custom_field_value['name']; ?></label>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'text') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>"
                             class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <input type="text"
                                       name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]"
                                       value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                       placeholder="<?php echo $custom_field['name']; ?>"
                                       id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                       class="form-control"/>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'textarea') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>"
                             class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <textarea
                                        name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]"
                                        rows="5" placeholder="<?php echo $custom_field['name']; ?>"
                                        id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                        class="form-control"><?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'file') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>"
                             class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <button type="button"
                                        id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                        data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i
                                            class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                <input type="hidden"
                                       name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]"
                                       value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : ''); ?>"/>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'date') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>"
                             class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text"
                                           name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]"
                                           value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                           placeholder="<?php echo $custom_field['name']; ?>"
                                           data-date-format="YYYY-MM-DD"
                                           id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="material-icons-event_note"></i></button>
                </span></div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'time') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>"
                             class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div class="input-group time">
                                    <input type="text"
                                           name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]"
                                           value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                           placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm"
                                           id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="material-icons-event_note"></i></button>
                </span></div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'datetime') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>"
                             class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-sm-2 control-label"
                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-sm-10">
                                <div class="input-group datetime">
                                    <input type="text"
                                           name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]"
                                           value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                           placeholder="<?php echo $custom_field['name']; ?>"
                                           data-date-format="YYYY-MM-DD HH:mm"
                                           id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="material-icons-event_note"></i></button>
                </span></div>
                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>

                    </fieldset>


                    <fieldset id="podpiska">
                        <legend class=" my-10"><?php echo $text_newsletter; ?></legend>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" style="text-align: left">Рассылка спецпредложений и
                                акций</label>
                            <div class="col-sm-8">
                                <?php if ($newsletter) { ?>
                                <label class="radio-inline">
                                    <input type="radio" name="newsletter" value="1" checked="checked"/>
                                    <?php echo $text_yes; ?></label>
                                <label class="radio-inline">
                                    <input type="radio" name="newsletter" value="0"/>
                                    <?php echo $text_no; ?></label>
                                <?php } else { ?>
                                <label class="radio-inline">
                                    <input type="radio" name="newsletter" value="1"/>
                                    <?php echo $text_yes; ?></label>
                                <label class="radio-inline">
                                    <input type="radio" name="newsletter" value="0" checked="checked"/>
                                    <?php echo $text_no; ?></label>
                                <?php } ?>
                            </div>
                        </div>
                    </fieldset>

                    <?php echo $captcha; ?>
                    <?php if ($text_agree) { ?>

                    <div class="buttons">
                        <div>
                            <?php if ($agree) { ?>
                            <input type="checkbox" name="agree" value="1" checked="checked"/>
                            <?php } else { ?></div>
                        <input type="checkbox" name="agree" value="1"/>
                        <?php } ?>
                        <?php echo $text_agree; ?>
                    </div>

                    <div class="row">
                        <div class="col-xs-12" style="margin-bottom: 5rem">
                            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary"/>
                        </div>
                    </div>
                </div>
                <?php } else { ?>
                <div class="buttons">
                    <div class="pull-right">
                        <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary"/>
                    </div>
                </div>
                <?php } ?>
        </div>
        </form>
        <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
    // Sort the custom fields
    $('#account .form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
            $('#account .form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('#account .form-group').length) {
            $('#account .form-group:last').after(this);
        }

        if ($(this).attr('data-sort') == $('#account .form-group').length) {
            $('#account .form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('#account .form-group').length) {
            $('#account .form-group:first').before(this);
        }
    });

    $('#address .form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
            $('#address .form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('#address .form-group').length) {
            $('#address .form-group:last').after(this);
        }

        if ($(this).attr('data-sort') == $('#address .form-group').length) {
            $('#address .form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('#address .form-group').length) {
            $('#address .form-group:first').before(this);
        }
    });

    $('input[name=\'customer_group_id\']').on('change', function () {
        $.ajax({
            url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
            dataType: 'json',
            success: function (json) {
                $('.custom-field').hide();
                $('.custom-field').removeClass('required');

                for (i = 0; i < json.length; i++) {
                    custom_field = json[i];

                    $('#custom-field' + custom_field['custom_field_id']).show();

                    if (custom_field['required']) {
                        $('#custom-field' + custom_field['custom_field_id']).addClass('required');
                    }
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('input[name=\'customer_group_id\']:checked').trigger('change');
    //--></script>
<script type="text/javascript"><!--
    $('button[id^=\'button-custom-field\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $(node).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
    //--></script>
<script src="/jquery.mask.js"></script>
<script type="text/javascript"><!--
    $('select[name=\'country_id\']').on('change', function () {
        $.ajax({
            url: 'index.php?route=account/account/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
                $('select[name=\'zone_id\']').selectbox('detach');
                $('select[name=\'zone_id\']').selectbox('attach');
            },
            success: function (json) {
                if (json['postcode_required'] == '1') {
                    // $('input[name=\'postcode\']').parent().parent().addClass('required');
                } else {
                    $('input[name=\'postcode\']').parent().parent().removeClass('required');
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'zone_id\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $(document).ready(function () {
        $('select[name=\'country_id\']').trigger('change');
        $('#close_ur').click(function () {
            $('#fact_addr input').prop('requared', false);
            $('#address').removeClass('in');
            $('#doc_request_ip, #doc_request_ur').hide();
            $( "#skype-inputs, #icq-inputs" ).removeClass('col-sm-6');
            $( "#skype-inputs, #icq-inputs" ).addClass('col-sm-4');
/* обязательность полей для физ*/
            // удал добавл док-тов ИНН ОГРНИП
            $('#download-documents-inputs, #input-inn, #input-ogrnip').removeAttr('required' , true);

            // Реквизиты организации( не ип): Наименование Форма организации ОГРН
            // Юридический адрес: юр страна  юр город юр адрес юр юр индекс
            // Фактический адрес:    факт страна   факт город  факт адрес факт индекс
            // Платежные реквизиты :   БИК Наименование банка Город банка Корреспондентский счет Расчетный счет
            $('#input-denomination, #input-company-type, #input-ogrn, #input-country, #input-city, #input-address-1, #input-postcode, #input-country-fact, #input-city_fact, #input-address-fact, #input-postcode_fact, #input-postcode_fact, #input-bic, #input-bank, #input-bank_city, #input-ks, #input-rs').removeAttr('required'  , true);

/* ..//обязательность полей для физ*/
        });

        jQuery('#same-with-ur').click(function () {
            var addr = jQuery('#fact_addr');

            if (addr.hasClass('in')) {
                addr.removeClass('in');
                jQuery('#fact_addr input').prop('required', false);
            } else {

                addr.addClass('in');
                jQuery('#fact_addr input').prop('required', true);
            }
        });

        $('#input-telephone').mask('+0 (000) 000-00-00');

        $('#open_ur').click(function () {
            var addr = jQuery('#fact_addr');
            var all = jQuery('#address');

            all.addClass('in');

            addr.addClass('in');

            $('#requizitus, #factical_address, #urid_address, #naimenovine_inputs, #kpp_inputs, #ogrn_inputs, #doc_request_ur').show();
            $('#ogrnip_inputs, #doc_request_ip').hide();
            $( "#skype-inputs, #icq-inputs" ).removeClass('col-sm-4');
            $( "#skype-inputs, #icq-inputs" ).addClass('col-sm-6');
            /* обязательность полей для юр*/
            //добавл док-тов
            $('#download-documents-inputs, #input-inn, #input-denomination, #input-company-type, #input-ogrn, #input-country, #input-city, #input-address-1, #input-postcode, #input-country-fact, #input-city_fact, #input-address-fact, #input-postcode_fact, #input-postcode_fact, #input-bic, #input-bank, #input-bank_city, #input-ks, #input-rs').attr('required'  , true);

            $('#input-ogrnip').removeAttr('required'  , true);
            /* обязательность полей для юр*/
        });

        $("input[type='submit']").click(function () {
            var $fileUpload = $("input[type='file']");
            if (parseInt($fileUpload.get(0).files.length) > 20) {
                alert("Вы можете загрузить не более 20 файлов!");
            }
        });

        $('#open_ip').click(function () {
            $('#fact_addr input').prop('requared', false);
            $('#address').addClass('in');
            $('#requizitus, #factical_address, #urid_address, #naimenovine_inputs, #kpp_inputs, #ogrn_inputs, #doc_request_ur').hide();
            $('#ogrnip_inputs, #doc_request_ip').show();
            $( "#skype-inputs, #icq-inputs" ).removeClass('col-sm-4');
            $( "#skype-inputs, #icq-inputs" ).addClass('col-sm-6');
            /* обязательность полей для ип*/

            $('#download-documents-inputs, #input-inn, #input-ogrnip').attr('required'  , true);

            $('#input-denomination, #input-company-type, #input-ogrn, #input-country, #input-city, #input-address-1, #input-postcode, #input-country-fact, #input-city_fact, #input-address-fact, #input-postcode_fact, #input-postcode_fact, #input-bic, #input-bank, #input-bank_city, #input-ks, #input-rs').removeAttr('required'  , true);
            /* обязательность полей для ип*/
        });

    });


    //--></script>
<?php echo $footer; ?>
