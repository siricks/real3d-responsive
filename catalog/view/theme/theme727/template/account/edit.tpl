<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger">
  <i class="fa fa-exclamation-circle"></i>
  <?php echo $error_warning; ?>
  <button type="button" class="close material-design-close47"></button>
  </div>
<?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="h1-after-m-0 "><?php echo $heading_title; ?></h1>

      <?php


foreach ($company_info as $key => &$company_infos){
      $clear = trim($company_infos);
      if (!empty($clear)) {
      $company_infos = $clear;
      } else {
      unset($company_info[$key]);
      }
      }

        //if ((isset($company_info['denomination'])) || (isset($company_info['company_type'])) || (isset($company_info['activity_type'])) ||  (isset($company_info[ 'inn'])) || (isset($company_info[ 'kpp'])) || (isset($company_info[ 'ogrn']))){
//$show_organisation_requisites == true }
     //       if(isset() ||){

     //       }

        if (isset($company_info) && count($company_info)) {


      $translates = [
    'company' => [
      'rus' => 'Наименование компании',
      'value' => $company_info['company'] ? $company_info['company'] : '',
      ],
      'activity_type' => [
      'rus' => 'Вид деятельности',
      'value' => $company_info['activity_type'],
      ],
      'specialization' => [
      'rus' => 'Специализация',
      'value' => $company_info['specialization'],
      ],
      'inn' => [
      'rus' => 'ИНН',
      'value' => $company_info['inn'],
      ],
      'kpp' => [
      'rus' => 'КПП',
      'value' => $company_info['kpp'],
      ],
      'ogrn' => [
      'rus' => 'ОГРН',
      'value' => $company_info['ogrn'],
      ],
      'okpo' => [
      'rus' => 'ОКПО',
      'value' => $company_info['okpo'],
      ],
      'okved' => [
      'rus' => 'ОКВЕД',
      'value' => $company_info['okved'],
      ],
      'bank' => [
      'rus' => 'БАНК',
      'value' => $company_info['bank'],
      ],
      'rs' => [
      'rus' => 'р/с',
      'value' => $company_info['rs'],
      ],
      'ks' => [
      'rus' => 'Корр.счет',
      'value' => $company_info['ks'],
      ],
      'company_type' => [
      'rus' => 'Форма<br>организации',
      'value' => $company_info['company_type'],
      ],


      'estimated_turnover' => [
      'rus' => 'Предполагаемый объем',
      'value' => $company_info['estimated_turnover'],
      ],

      'denomination' => [
      'rus' => 'Наименование',
      'value' => $company_info['denomination'],
      ],

      'city_fact' => [
      'rus' => 'Фактический город',
      'value' => $company_info['city_fact'],
      ],

      'address_fact' => [
      'rus' => 'Фактический адрес',
      'value' => $company_info['address_fact'],
      ],

      'postcode_fact' => [
      'rus' => 'Фактический индекс',
      'value' => $company_info['postcode_fact'],
      ],

      'bic' => [
      'rus' => 'БИК',
      'value' => $company_info['bic'],
      ],


      'bank_city' => [
      'rus' => 'Город банка',
      'value' => $company_info['bank_city'],
      ],

      'sex' => [
      'rus' => 'Пол',
      'value' => $company_info['sex'],
      ],


      'birthday' => [
      'rus' => 'Дата рождения',
      'value' => $company_info['birthday'],
      ],

      'skype' => [
      'rus' => 'Скайп',
      'value' => $company_info['skype'],
      ],

      'icq' => [
      'rus' => 'ICQ',
      'value' => $company_info['icq'],
      ],

      'country_id_fact' => [
      'rus' => 'Фактическая страна',
      'value' => $company_info['country_id_fact'],
      ],

      ];
      }

      ?>


      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="edit-form-tpl form-horizontal">


        <div class="col-sm-12">
          <!--*******Основные данные********-->
          <!-- +++ Имя--><!--Фамилия--><!--Отчество-->
          <!--  +++ E-mail:--><!--Пароль--><!--Подтверждение пароля-->
          <!-- ++ Телефон--><!--Предполагаемый оборот-->
          <fieldset class="row" id="main-info">
            <div class="col-sm-4"><legend>Основные данные</legend></div>
            <!-- +++ Имя--><!--Фамилия--><!--Отчество-->
            <div class="col-sm-12"><div class="row">

                <!--Имя-->

                <div class="col-sm-4 px-30 my-10"><div class="row">
                    <label class="col-sm-12" for="input-firstname">Имя</label>
                    <input class="col-sm-12" type="text" name="firstname" value="<?php echo $firstname; ?>" id="input-firstname" required>
                    <?php if ($error_firstname) { ?>
                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                    <?php } ?>
                  </div></div>

                <!--Фамилия-->
                <div class="col-sm-4 px-30 my-10"><div class="row row-with-select">
                    <label class="col-sm-12" for="input-lastname">Фамилия</label>
                    <input class="col-sm-12" type="text" name="lastname" value="<?php echo $lastname; ?>" id="input-lastname" required>
                    <?php if ($error_lastname) { ?>
                    <div class="text-danger"><?php echo $error_lastname; ?></div>
                    <?php } ?>
                  </div></div>

                <!--Отчество-->
                    <?php if (isset($secondname)) { ?>
                <div class="col-sm-4 px-30 my-10"><div class="row">
                    <label class="col-sm-12" for="input-secondname">Отчество</label>
                    <input class="col-sm-12" type="text" name="secondname" value="<?php echo $secondname; ?>" id="input-secondname" required>
                    <?php if ($error_secondname) { ?>
                    <div class="text-danger"><?php echo $error_secondname; ?></div>
                    <?php } ?>
                  </div></div>
                    <?php } ?>

            <!--  +++ E-mail:--><!--Пароль--><!--Подтверждение пароля-->

                <!--E-mail:-->
                <div class="col-sm-4 px-30 my-10"><div class="row">
                    <label class="col-sm-12" for="input-email">E-mail:</label>
                    <input class="col-sm-12" type="email" name="email" value="<?php echo $email; ?>" id="input-email" >
                    <?php if ($error_email) { ?>
                    <div class="text-danger"><?php echo $error_email; ?></div>
                    <?php } ?>
                  </div></div>
                <!--Пароль
                <div class="col-sm-4 px-30 my-10"><div class="row">
                    <label class="col-12" for="input-password">Пароль</label>
                    <input class="col-12" type="password"  name="password" value="<?php echo $password; ?>" id="input-password" required>
                    <?php if ($error_password) { ?>
                    <div class="text-danger"><?php echo $error_password; ?></div>
                    <?php } ?>
                  </div></div>
                Подтверждение пароля
                <div class="col-sm-4 px-30 my-10"><div class="row">
                    <label class="col-sm-12" for="input-confirm">Подтверждение пароля</label>
                    <input class="col-sm-12" type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" required>
                    <?php if ($error_confirm) { ?>
                    <div class="text-danger"><?php echo $error_confirm; ?></div>
                    <?php } ?>
                  </div></div>-->
              </div>
            </div>
            <!-- ++ Телефон--><!--Предполагаемый оборот-->
            <div class="col-sm-12"><div class="row">
                <!-- Телефон -->
                <div class="col-sm-6 px-30 my-10">
                  <div class="row">
                    <label class="col-sm-12" for="input-telephone">Телефон</label>
                    <input class="col-sm-12" type="tel" id="input-telephone" value="<?php echo $telephone; ?>" placeholder="+7 (987) 123-45-67" name="telephone" >
                    <?php if ($error_telephone) { ?>
                    <div class="text-danger"><?php echo $error_telephone; ?></div>
                    <?php } ?>
                  </div></div>

                <!--Предполагаемый оборот-->
                    <?php if (isset($company_info[ 'estimated_turnover'])) { ?>
                <div class="col-sm-6 px-30 my-10"><div class="row">
                    <label class="col-sm-12" for="estimated_turnover">Предполагаемый оборот</label>
                    <input class="col-sm-12" type="text" value="<?php echo $company_info[ 'estimated_turnover']; ?>" name="estimated_turnover" id="estimated_turnover" >
                  </div></div>
<?php } ?>
              </div></div>
          </fieldset>

       <?php // if ( $fizlico == false) { ?>
          <div class="row"  id="address"><div class="col-sm-12">
 <?php if ((isset($company_info['denomination'])) || (isset($company_info['company_type'])) || (isset($company_info['activity_type'])) ||  (isset($company_info[ 'inn'])) || (isset($company_info[ 'kpp'])) || (isset($company_info[ 'ogrn']))){ ?>
              <!--*******Реквизиты организации********-->
              <!--+++Наименование--><!--Форма организации--><!--Вид деятельности-->
              <!--+++ ИНН --><!-- КПП --><!-- ОГРН -->
              <fieldset class="row" id="organization-requisites">
                <div class="col-sm-4 mt-10"><legend>Реквизиты организации</legend></div>

                <!--+++Наименование--><!--+++Форма организации--><!--+++Вид деятельности-->
                <div class="col-sm-12"><div class="row">

                    <!--Наименование-->
                        <?php if (isset($company_info['denomination'])) { ?>
                    <div class="col-sm-12 col-md-4 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-denomination">Наименование</label>
                        <input class="col-sm-12" type="text" value="<?php echo $company_info[ 'denomination']; ?>" name="denomination" id="input-denomination" >
                      </div></div>
                        <?php } ?>

                    <!--Форма организации+-->
                        <?php if (isset($company_info['company_type'])) { ?>
                    <div class="col-sm-12 col-md-4 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-company-type1">Форма организации</label>
                        <select class="col-sm-12"   name="company_type" id="input-company-type">
                          <option  disabled <?php if ($company_info['company_type'] == ''){ echo 'selected' ; } ?> value="">Выберите форму организации</option>
                          <option <?php if ($company_info['company_type'] == 'ИП'){ echo 'selected' ; } ?> value="ИП">ИП</option>
                          <option <?php if ($company_info['company_type'] == 'ЗАО'){ echo 'selected' ; } ?> value="ЗАО">ЗАО</option>
                          <option <?php if ($company_info['company_type'] == 'ООО'){ echo 'selected' ; } ?> value="ООО">ООО</option>
                          <option <?php if ($company_info['company_type'] == 'ТОО'){ echo 'selected' ; } ?> value="ТОО">ТОО</option>
                          <option <?php if ($company_info['company_type'] == 'ОАО'){ echo 'selected' ; } ?> value="ОАО">ОАО</option>
                        </select>
                      </div></div>
                        <?php } ?>


                        <!--ОКПО-->
                        <?php if (isset($company_info['okpo'])) { ?>
                        <div class="col-sm-4 px-30 my-10"><div class="row">
                                <label class="col-sm-12"  for="input-okpo">ОКПО</label>
                                <input class="col-sm-12" type="text" name="okpo" value="<?php echo $company_info['okpo']; ?>" id="input-okpo" >
                            </div></div>
                        <?php } ?>


                <!--+++ ИНН --><!-- КПП --><!-- ОГРН -->

                    <!-- ИНН -->
                        <?php if (isset($company_info[ 'inn'])) { ?>
                    <div class="col-sm-4 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-inn">ИНН</label>
                        <input class="col-sm-12" type="text" name="inn" value="<?php echo $company_info[ 'inn']; ?>"  id="input-inn" >
                      </div></div>
                        <?php } ?>

                    <!-- КПП -->
                        <?php if (isset($company_info[ 'kpp'])) { ?>
                    <div class="col-sm-4 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-kpp">КПП</label>
                        <input class="col-sm-12" value="<?php echo $company_info[ 'kpp']; ?>" type="text" name="kpp" id="input-kpp" >
                      </div></div>
                        <?php } ?>

                    <!-- ОГРН -->
                        <?php if (isset($company_info[ 'ogrn'])) { ?>
                    <div class="col-sm-4 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-ogrn">ОГРН</label>
                        <input class="col-sm-12"  type="text" name="ogrn" value="<?php echo $company_info[ 'ogrn']; ?>" id="input-ogrn" >
                      </div></div>
                        <?php } ?>

                        <!-- ОГРНИП -->
                        <?php if (isset($company_info[ 'ogrnip'])) { ?>
                        <div class="col-sm-4 px-30 my-10"><div class="row">
                                <label class="col-sm-12" for="input-ogrnip">ОГРНИП</label>
                                <input class="col-sm-12"  type="text" name="ogrnip" value="<?php echo $company_info[ 'ogrnip']; ?>" id="input-ogrnip" >
                            </div></div>
                        <?php } ?>

                  </div>
                </div>
              </fieldset>
<?php } ?>
                  <?php  // } ?>


                  <?php

                   if (  (isset($city)) || (isset($address_1)) || (isset($postcode)) ){ ?>
              <!--*******Юридический адрес********-->
              <!--++Страна--><!--Город-->
              <!--++Юридический Адрес--> <!--Индекс-->
              <fieldset class="row">
                <div class="col-sm-4 mt-10"><legend>Юридический адрес</legend></div>

                <div class="col-sm-12"><div class="row">

                    <!--Страна-->
                        <?php if (isset($country['country_id'])) { ?>
                    <div class="col-sm-6 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-country">Страна</label>
                        <select name="country_id" id="input-country" class="col-sm-12">
                          <option value=""><?php echo $text_select; ?></option>
                          <?php foreach ($countries as $country) { ?>
                          <?php if ($country['country_id'] == $country_id) { ?>
                          <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select>
                        <?php if ($error_country) { ?>
                        <div class="text-danger"><?php echo $error_country; ?></div>
                        <?php } ?>
                      </div></div>
                        <?php } ?>

                    <!--Город-->
                        <?php if (isset($city)){ ?>
                    <div class="col-sm-6 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-city">Город</label>
                        <input class="col-sm-12" type="text" name="city" value="<?php echo $city; ?>" id="input-city">
                        <?php if ($error_city) { ?>
                        <div class="text-danger"><?php echo $error_city; ?></div>
                        <?php } ?>
                      </div></div>
                        <?php } ?>
                    <!--
                    TODO:  Город если здесь должен быть регион то раскоментить

                    <div class="col-sm-6 px-30"><div class="row">
                        <label class="col-sm-12" for="input-zone">Город</label>
                        <select name="zone_id" id="input-zone" class="col-sm-12"></select>
                      </div></div>-->


                    <!--Юридический Адрес-->
                        <?php if (!empty($address_1)) { ?>
                    <div class="col-sm-6 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-address-1">Юридический Адрес</label>
                        <input class="col-sm-12" type="text" name="address_1" value="<?php echo $address_1; ?>" id="input-address-1">
                        <?php if ($error_address_1) { ?>
                        <div class="text-danger"><?php echo $error_address_1; ?></div>
                        <?php } ?>
                      </div></div>
                        <?php } ?>

                    <!--Индекс-->
                        <?php if (isset($postcode)) { ?>
                    <div class="col-sm-6 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-postcode">Индекс:</label>
                        <input class="col-sm-12" type="text"  name="postcode" value="<?php echo $postcode; ?>" id="input-postcode"  >
                        <?php if ($error_postcode) { ?>
                        <div class="text-danger"><?php echo $error_postcode; ?></div>
                        <?php } ?>
                      </div></div>
                        <?php }  ?>

                  </div></div>

              </fieldset>
                  <?php } ?>


                  <?php if (  (isset($company_info['city_fact'])) || (isset($company_info['address_fact'])) || (isset($company_info['postcode_fact'])) ){ ?>
                  <!--*******Фактический адрес********-->
              <!--Совпадает с юридическим адресом-->
              <!--++Страна--><!--Город-->
              <!--++Фактический адрес--> <!--Индекс-->
              <fieldset class="row">
                <div class="col-sm-4 mt-10"><legend>Фактический адрес</legend></div>


                <div class="col-sm-12 collapse in" id="fact_addr"><div class="row">

                    <div class="col-sm-12"><div class="row">

                        <!--Страна-->

                            <?php

                            if (isset($company_info['country_id_fact'])) { ?>
                        <div class="col-sm-6 px-30 my-10"><div class="row">
                            <label class="col-sm-12" for="input-country-fact">Страна</label>
                            <select name="country_id-fact" id="input-country-fact" class="col-sm-12">
                              <option value=""><?php echo $text_select; ?></option>
                              <?php foreach ($countries as $country) { ?>
                              <?php if ($country['country_id'] == $country_id-fact) { ?>
                              <option value="<?php echo $country['country_id-fact']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                              <?php } else { ?>
                              <option value="<?php echo $country['country_id-fact']; ?>"><?php echo $country['name']; ?></option>
                              <?php } ?>
                              <?php } ?>
                            </select>
                          </div></div>
                            <?php } ?>

                        <!--Город-->
                            <?php if (isset($company_info['city_fact'])) { ?>
                        <div class="col-sm-6 px-30 my-10"><div class="row">
                            <label class="col-sm-12" for="input-city_fact">Город</label>
                            <input class="col-sm-12" type="text" name="city_fact" value="<?php echo $company_info['city_fact']; ?>" id="input-city_fact">
                          </div></div>
                            <?php } ?>


                        <!--Фактический адрес-->
                            <?php if (isset($company_info['address_fact'])) { ?>
                        <div class="col-sm-6 px-30 my-10"><div class="row">
                            <label class="col-sm-12" for="input-address-fact">Фактический адрес</label>
                            <input class="col-sm-12" type="text" name="address_fact" value="<?php echo $company_info['address_fact']; ?>" id="input-address-fact">
                          </div></div>
                            <?php } ?>

                        <!--Индекс-->
                            <?php if (isset($company_info['postcode_fact'])) { ?>
                        <div class="col-sm-6 px-30 my-10"><div class="row">
                            <label class="col-sm-12" for="input-postcode_fact">Индекс:</label>
                            <input class="col-sm-12" type="text"  name="postcode_fact" value="<?php echo $company_info['postcode_fact']; ?>" id="input-postcode_fact"  >
                          </div></div>
                            <?php } ?>
                      </div></div>

                  </div></div>

              </fieldset>
                  <?php } ?>


                  <?php  if (  (isset($company_info['bic']))  || (isset($company_info['bank'])) || (isset($company_info['bank_city'])) || (isset($company_info['ks'])) || (isset($company_info['rs'])) ){ ?>
              <!--*******Платежные реквизиты*******-->
              <!--++БИК--><!--+++Наименование банка:-->
              <!--+++Город банка:--><!--++Корреспондентский счет:--><!--+++Расчетный счет:-->
              <fieldset class="row">
                <div class="col-sm-4 mt-10"><legend>Платежные реквизиты</legend></div>

                <div class="col-sm-12"><div class="row">

                    <!--БИК-->
                        <?php if (isset($company_info['bic'])) { ?>
                    <div class="col-sm-6 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-bic">БИК</label>
                        <input class="col-sm-12" type="text" name="bic" value="<?php echo $company_info['bic']; ?>" id="input-bic" " >
                      </div></div>
                        <?php } ?>

                    <!--Наименование банка:-->
                        <?php if (isset($company_info['bank'])) { ?>
                    <div class="col-sm-6 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-bank">Наименование банка:</label>
                        <input class="col-sm-12" type="text" name="bank" value="<?php echo $company_info['bank']; ?>" id="input-bank" " >
                      </div></div>
                        <?php } ?>

                  </div></div>

                <div class="col-sm-12"><div class="row">
                    <!--Город банка:-->
                        <?php if (isset($company_info['bank_city'])) { ?>
                    <div class="col-sm-6 col-md-4 px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-bank_city">Город банка</label>
                        <input class="col-sm-12" type="text" name="bank_city" value="<?php echo $company_info['bank_city']; ?>" id="input-bank_city" >
                      </div></div>
                        <?php } ?>

                    <!--Корреспондентский счет:-->
                        <?php if (isset($company_info['ks'])) { ?>
                    <div class="col-sm-6 col-md-4  px-30 my-10"><div class="row">
                        <label class="col-sm-12" for="input-ks">Корреспондентский счет</label>
                        <input class="col-sm-12" type="text" name="ks" value="<?php echo $company_info['ks']; ?>" id="input-ks"  >
                      </div></div>
                        <?php } ?>

                    <!--Расчетный счет:-->
                        <?php if (isset($company_info['rs'])) { ?>
                    <div class="col-sm-6 col-md-4 px-30 my-10"><div class="row">
                        <label class="col-sm-12"  for="input-rs">Расчетный счет</label>
                        <input class="col-sm-12" type="text" name="rs" value="<?php echo $company_info['rs']; ?>" id="input-rs" >
                      </div></div>
                        <?php } ?>
                  </div></div>

              </fieldset>
                  <?php  } ?>
            </div></div>


          <!--******Дополнительная информация*******-->
          <fieldset class="row">
            <div class="col-sm-5 mt-10"><legend>Дополнительная информация</legend></div>

            <div class="col-sm-12"><div class="row">

                <!--Пол-->
                    <?php if (isset($company_info['sex'])) {
?>
                <div class="col-sm-4 px-30 my-10"><div class="row">
                    <label class="col-sm-12">Пол</label>
                    <select name="sex" id="input-sex">
                      <option  disabled <?php if ($company_info['sex'] == ''){ echo 'selected' ; } ?> value="">Пол</option>
                      <option <?php if ($company_info['sex'] == 'Мужской'){ echo 'selected' ; } ?>  value="Мужской">Мужской</option>
                      <option <?php if ($company_info['sex'] == 'Женский'){ echo 'selected' ; } ?> value="Женский">Женский</option>
                    </select>
                  </div></div>
                    <?php } ?>

                <!--Дата рождения-->
                    <?php if (isset($company_info['birthday'])) { ?>
                <div class="col-sm-4 px-30 my-10"><div class="row">
                    <label class="col-sm-12" for="input-birthday">Дата рождения</label>
                    <input class="col-sm-12" type="date" value="<?php echo $company_info['birthday']; ?>"  name="birthday" id="input-birthday" >
                  </div></div>
                    <?php } ?>

                    <!--Вид деятельности+-->
                    <?php if (isset($company_info['activity_type'])) { ?>
                    <div class="col-sm-4 px-30 my-10"><div class="row">
                            <label class="col-sm-12" for="input-activity-type">Вид деятельности</label>
                            <select name="activity_type" id="input-activity-type">
                                <option disabled  <?php if ($company_info['activity_type'] == ''){ echo 'selected' ; } ?> value="">выберите виды деятельности </option>
                                <option  <?php if ($company_info['activity_type'] == 'СТО'){ echo 'selected' ; } ?> value="СТО">СТО</option>
                                <option <?php if ($company_info['activity_type'] == 'АПТ'){ echo 'selected' ; } ?> value="АПТ">АПТ</option>
                                <option <?php if ($company_info['activity_type'] == 'Магазин'){ echo 'selected' ; } ?> value="Магазин">Магазин</option>
                                <option <?php if ($company_info['activity_type'] == 'сеть магазинов'){ echo 'selected' ; } ?> value="сеть магазинов">сеть магазинов</option>
                                <option <?php if ($company_info['activity_type'] == 'производитель техники'){ echo 'selected' ; } ?> value="производитель техники">производитель техники</option>
                            </select>
                        </div></div>
                    <?php } ?>

                <!--Skype-->
                    <?php if (isset($company_info['skype'])) { ?>
                <div class="col-sm-4 px-30 my-10"><div class="row">
                    <label class="col-sm-12">Skype</label>
                    <input class="col-sm-12" type="text" value="<?php echo $company_info['skype']; ?>"  id="input-skype" name="skype" >
                  </div></div>
                    <?php } ?>

                <!--ICQ-->

                    <?php if (isset($company_info['icq'])) { ?>
                <div class="col-sm-4 px-30 my-10"><div class="row">
                    <label class="col-sm-12" for="input-icq">ICQ</label>
                    <input class="col-sm-12" value="<?php echo $company_info['icq']; ?>" type="text" name="icq" id="input-icq" >
                  </div></div>
                    <?php } ?>

                <!--Документы -->

                <?php if($company_info['documets']): $company_info['documets'] = unserialize($company_info['documets']); ?>
                    <div class="col-sm-4 px-30 my-10"><div class="row">
                        <label class="col-sm-12">Добавленные документы</label>
                            <?php if( is_array($company_info['documets']) ): foreach ($company_info['documets'] as $doc) :?>
                            <a href="/image/data/<?=$doc;?>" target="_blank"><?=$doc;?></a>
                            <?php endforeach; endif; ?>

                      </div></div>
                <?php endif; ?>


              </div></div>

           <!--  <div class="col-sm-12 px-30"><div class="row"><label class="col-12  my-10" for="sendtext">Отправить сообщение</label><textarea id="sendtext" name="sendtext"  class="col-12" rows="4"></textarea></div></div>-->
          </fieldset>
        </div>




        <div class="buttons clearfix">
          <!-- <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>-->
          <div class="pull-right">
            <input type="submit" value="Сохранить" class="btn btn-primary" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
		$('.form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('.form-group').length) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('.form-group').length) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('.form-group').length) {
		$('.form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script>
<?php echo $footer; ?>