<?php
class ControllerExtensionModuleTMNav extends Controller {
	public function index() {
		$this->load->language('common/header');
		
		if(($this->config->has('config_simple_blog_status')) && ($this->config->get('config_simple_blog_status'))) {
		$data['simple_blog_found'] = 1;
		$tmp = $this->config->get('config_simple_blog_footer_heading');
		if (!empty($tmp)) {
		$data['simple_blog_footer_heading'] = $this->config->get('config_simple_blog_footer_heading');
		} else {
		$data['simple_blog_footer_heading'] = $this->language->get('text_simple_blog');
		}
		$data['simple_blog']	= $this->url->link('simple_blog/article');
		}
		
		$data['text_home']          = $this->language->get('text_home');
		$data['text_account']       = $this->language->get('text_account');
		$data['text_order']         = $this->language->get('text_order');
		$data['text_transaction']   = $this->language->get('text_transaction');
		$data['text_download']      = $this->language->get('text_download');
		$data['text_logout']        = $this->language->get('text_logout');
		$data['text_register']      = $this->language->get('text_register');
		$data['text_login']         = $this->language->get('text_login');
		$data['text_compare']       = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_checkout']      = $this->language->get('text_checkout');

		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');
			
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['logged']        = $this->customer->isLogged();
		$data['home']          = $this->url->link('common/home');
		$data['account']       = $this->url->link('account/account', '', true);
		$data['order']         = $this->url->link('account/order', '', true);
		$data['transaction']   = $this->url->link('account/transaction', '', true);
		$data['download']      = $this->url->link('account/download', '', true);
		$data['logout']        = $this->url->link('account/logout', '', true);
		$data['register']      = $this->url->link('account/register', '', true);
		$data['login']         = $this->url->link('account/login', '', true);
		$data['compare']       = $this->url->link('product/compare');
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout']      = $this->url->link('checkout/checkout', '', true);
		$data['wishlist']      = $this->url->link('account/wishlist', '', true);
		$data['telephone']     = $this->config->get('config_telephone');

		$data['open_text'] = $this->language->get('open_text');

        $this->load->language('common/cart');

        // Totals
        $this->load->model('extension/extension');

        $totals = array();
        $taxes = $this->cart->getTaxes();
        $total = 0;

        // Because __call can not keep var references so we put them into an array.
        $total_data = array(
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$total
        );

        // Display prices
        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);

                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            $sort_order = array();

            foreach ($totals as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $totals);
        }


        $data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));


        $data['text_items2'] = sprintf($this->language->get('text_items2'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));





        return $this->load->view('extension/module/tm_nav', $data);
	}
}